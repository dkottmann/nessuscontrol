__author__ = 'Dan Kottmann <djkottmann@gmail.com>'

from distutils.core import setup

setup(
    name="NessusControl",
    version="0.0.3",
    author='Dan Kottmann',
    author_email='djkottmann@gmail.com',
    packages=['nessus_control'],
    scripts=['bin/nessus_control'],
    url='https://bitbucket.org/dkottmann/nessus_control',
    license='LICENSE.txt',
    description='Nessus XML RPC interface for managing scans.',
    install_requires=[
        "requests >= 1.2.3",
        "docopt >= 0.6.1"
    ],
)