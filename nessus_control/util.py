#!/usr/bin/env python
# Copyright (c) 2013 Dan Kottmann
# See the file license.txt for copying permission


def croak(data):
    """
    Display an error message and abort
    """
    print error("[!] Error: {0}".format(data))
    exit(1)


def warning(data):
    """
    format a string for 'yellow' colored terminal output
    """
    return '\033[93m' + data + '\033[0m'


def error(data):
    """
    format a string for 'red' colored terminal output
    """
    return '\033[91m' + data + '\033[0m'


def success(data):
    """
    format a string for 'green' colored terminal output
    """
    return '\033[92m' + data + '\033[0m'
