__author__ = 'Dan Kottmann <djkottmann@gmail.com>'

import requests
from xml.etree import ElementTree
from nessus_control.exceptions import ScanNotFoundError, PolicyNotFoundError,\
    PermissionDeniedError, ReportNotFoundError


def check_auth(fn):
    def wrapper(self, *args):
        if not self.auth_token:
            self.login()

        if not self.auth_token:
            raise PermissionDeniedError()

        return fn(self, *args)
    return wrapper


class NessusController():

    def __init__(self, host, user, password):
        self.user = user
        self.password = password
        self.host = host
        if not self.host.endswith('/'):
            self.host += '/'
        self.seq = "123456"
        self.auth_token = ""
        self.headers = {"Content-type": "application/x-www-form-urlencoded",
                        "Accept": "text/plain"}

    def _make_cookie(self):
        return dict(token=self.auth_token)

    def _is_ok(self, elem_tree):
        status = 'ERROR'
        elem = elem_tree.find('status')
        if elem is not None:
            status = elem.text

        return True if status == 'OK' else False

    def _send_post_request(self, url, payload=None):
        headers = self.headers
        if not payload:
            payload = dict()
        payload['seq'] = self.seq
        cookies = self._make_cookie()
        return requests.post(url, data=payload, headers=headers,
                             cookies=cookies, verify=False)

    def _get_scan_uuid(self, name):
        ret = None
        url = self.host + "scan/list"
        r = self._send_post_request(url)
        t = ElementTree.fromstring(r.content)
        if self._is_ok(t):
            scans = t.iter('scan')
            if scans is not None:
                for scan in scans:
                    uuid = scan.find('uuid').text
                    scan_name = scan.find('readableName').text
                    if name == scan_name:
                        ret = uuid
                        break
                else:
                    # Can't find scan by that name
                    ret = None

            return ret

    def _get_policy_id(self, name):
        ret = None
        url = self.host + "policy/list"
        r = self._send_post_request(url)
        t = ElementTree.fromstring(r.content)
        if self._is_ok(t):
            policies = t.iter('policy')
            if policies is not None:
                for policy in policies:
                    id = policy.find('policyID').text
                    policy_name = policy.find('policyName').text
                    if name == policy_name:
                        ret = id
                        break
                else:
                    # Can't find policy by that name
                    ret = None

            return ret

        return None

    def _get_report_uuid(self, name):
        ret = None
        url = self.host + "report/list"
        r = self._send_post_request(url)
        t = ElementTree.fromstring(r.content)
        if self._is_ok(t):
            reports = t.iter('report')
            if reports is not None:
                for report in reports:
                    uuid = report.find('name').text
                    report_name = report.find('readableName').text
                    if name == report_name:
                        ret = uuid
                        break
                else:
                    # Can't find report by that name
                    ret = None

            return ret

        return None

    def login(self):
        url = self.host + 'login'
        payload = {'login': self.user, 'password': self.password}
        r = self._send_post_request(url, payload)

        tree = ElementTree.fromstring(r.content)

        if not self._is_ok(tree):
            return False

        elem = tree.iter('token')
        token = ""
        if elem is not None:
            token = list(elem)[0].text

        if not token:
            return False

        self.auth_token = token
        return True

    def logout(self):
        url = self.host + "logout"
        r = self._send_post_request(url)
        t = ElementTree.fromstring(r.content)
        return self._is_ok(t)

    @check_auth
    def stop_scan(self, scan_name):
        url = self.host + "scan/stop"
        uuid = self._get_scan_uuid(scan_name)
        if not uuid:
            raise ScanNotFoundError(scan_name)

        payload = {'scan_uuid': uuid}
        r = self._send_post_request(url, payload)
        t = ElementTree.fromstring(r.content)
        return self._is_ok(t)

    @check_auth
    def update_policy(self, policy_name, ports):
        url = self.host + "policy/edit"
        id = self._get_policy_id(policy_name)
        if not id:
            raise PolicyNotFoundError(policy_name)

        payload = {'policy_id': id, 'policy_name': policy_name,
                   'port_range': ports}
        r = self._send_post_request(url, payload)
        t = ElementTree.fromstring(r.content)
        return self._is_ok(t)

    @check_auth
    def start_scan(self, targets, policy_name, scan_name):
        url = self.host + "scan/new"
        policy_id = self._get_policy_id(policy_name)
        if not policy_id:
            raise PolicyNotFoundError(policy_name)

        payload = {'target': targets, 'policy_id': policy_id,
                   'scan_name': scan_name}
        r = self._send_post_request(url, payload)
        t = ElementTree.fromstring(r.content)
        return self._is_ok(t)

    @check_auth
    def pause_scan(self, scan_name):
        url = self.host + "scan/pause"
        uuid = self._get_scan_uuid(scan_name)
        if not uuid:
            raise ScanNotFoundError(scan_name)

        payload = {'scan_uuid': uuid}
        r = self._send_post_request(url, payload)
        t = ElementTree.fromstring(r.content)
        return self._is_ok(t)

    @check_auth
    def resume_scan(self, scan_name):
        url = self.host + "scan/resume"
        uuid = self._get_scan_uuid(scan_name)
        if not uuid:
            raise ScanNotFoundError(scan_name)

        payload = {'scan_uuid': uuid}
        r = self._send_post_request(url, payload)
        t = ElementTree.fromstring(r.content)
        return self._is_ok(t)

    @check_auth
    def get_scan_status(self, name):
        ret = "stopped"
        url = self.host + "scan/list"
        r = self._send_post_request(url)
        t = ElementTree.fromstring(r.content)
        if self._is_ok(t):
            scans = t.iter('scan')
            if scans is not None:
                for scan in scans:
                    status = scan.find('status').text
                    scan_name = scan.find('readableName').text
                    if name == scan_name:
                        ret = status
                        break

        return ret

    @check_auth
    def download_report(self, scan_name, outfile):
        url = self.host + "file/report/download"
        uuid = self._get_report_uuid(scan_name)
        if not uuid:
            raise ReportNotFoundError(scan_name)

        payload = {'report': uuid}
        r = self._send_post_request(url, payload)
        if not r or not r.content:
            return False

        # Write the file to disk
        with open(outfile, "w+") as fd:
            fd.write(r.content)

        return True

