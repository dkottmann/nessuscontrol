__author__ = 'Dan Kottmann <djkottmann@gmail.com>'


class ScanNotFoundError(Exception):

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Scan {0} not found".format(repr(self.name))


class PolicyNotFoundError(Exception):

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Policy {0} not found".format(repr(self.name))


class ReportNotFoundError(Exception):

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Report {0} not found".format(repr(self.name))


class PermissionDeniedError(Exception):

    def __str__(self):
        return "Permission denied. Authentication or admin rights required"
