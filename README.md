#NessusControl#

NessusControl is a python module and script capable of performing limited command-line management of
Nessus scans on local or remote systems via the Nessus XML RPC interface. Requires Python >= 2.7 required.

Execution is simple. Create a config file that contains pertinent information regarding the host, user, and password
to use when connecting to Nessus. THen just run the script passing the correct parameters to achieve the desired result.

Sample run:

        bash-3.2# python ./nessus_control start --config scanner.cfg --policy Custom --scan Internal --targets 10.0.100.0/24
        [+] Reading config file /Users/anykey/scanner.cfg...
        [+] Attempting to authenticate...
        [+] Authentication successful.
        [+] Attempting to start a new scan...
        [+] Scan successfully started
        [+] Attempting to logout...
        [+] Successfully logged out.

Sample config file:

        [nessus]
        host=https://10.1.1.100:8834
        user=nessus
        password=s3cr3t

Usage:

        nessus_control --help

        Usage:
          nessus_control start --config FILE --policy POLICY_NAME --scan SCAN_NAME --targets HOSTS
          nessus_control pause --config FILE --scan SCAN_NAME
          nessus_control stop --config FILE --scan SCAN_NAME
          nessus_control update-policy --config FILE --policy POLICY_NAME --ports PORTS
          nessus_control report --config FILE --scan SCAN_NAME --outfile FILE

        Commands:
          Only a single command may be specified:
          start                 Start a scan of the associated targets using a provided policy
          pause                 Pause a scan
          stop                  Stop a scan
          update-policy         Update the target ports for a given Nessus Policy
          report                Download the .nessus XML report for given scan

        Options:
          --config FILE         Full path to the configuration file to use
          --scan SCAN_NAME      The name of the scan as used in Nessus
          --ports PORTS         A CSV list of ports
          --targets HOSTS       A CSV list of hosts, networks, and ranges to scan
          --policy POLICY_NAME  The name of the policy as used in Nessus
          --outfile FILE        File to which output will be saved

###Installation###
1. Install pip
2. Install NessusControl with pip using the dist tarball

        sudo pip install NessusControl_<version>.tar.gz

###Running###

1. Execute nessus_control via the command line

        nessus_control [options]

2. View usage details

        nessus_control --help

